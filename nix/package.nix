{python3Packages}:
python3Packages.buildPythonPackage {
  pname = "frontend";
  version = "0.1";

  src = builtins.path {
    path = ../.;
    name = "source";
  };

  propagatedBuildInputs = with python3Packages; [flask requests];

  format = "pyproject";
  nativeBuildInputs = [python3Packages.setuptools];
}
