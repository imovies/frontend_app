# Front End
## Running The App

```bash
python main.py
```

## Viewing The App

Go to `http://127.0.0.1:5000`

## General to do:
- use https (apache config)
- generate correct secret key (in file  `_init_.py`)
- Make request to backend
- Remove debug mode 
- Print user info (+admin info if admin) on home page when you get them from back up 
-remove JINJA control caratere from user info before rendering
