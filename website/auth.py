from flask import Blueprint, render_template, request, flash, redirect, url_for, session
from werkzeug.security import generate_password_hash, check_password_hash
import requests

auth = Blueprint('auth', __name__)
bendAd = "http://backend"

@auth.route('/loginPWD', methods=['GET', 'POST'])
def loginPWD():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')

        ################################################
        ################################################
        ################################################
        ## Request to back end to check user info
        ## If success add session info in the cookie : session[’username’] = request.form[’username’]
        ##if error flash an error message: flash('User already exist', category='error')
        ################################################
        ################################################
        ################################################
        args = {"email" : email,
                "password" : password}
        res = requests.post(bendAd + "/login", json = args)
        if not res.ok:
            flash('could not connect with the given credentials', category = 'error')
            return render_template("loginPWD.html")

        session['uid'] = res.json()['uid']
        session['firstname'] = res.json()['firstname']
        session['lastname'] = res.json()['lastname']
        session['email'] = res.json()['email']
        session['is_admin'] = "False"

        return redirect(url_for('views.home'))


    return render_template("loginPWD.html")
    
@auth.route('/loginCert', methods=['GET', 'POST'])
def loginCert():
    if request.method == 'POST':
        user_certificate= request.form.get('Certificate')
        
        res = requests.post(bendAd + "/cert", json = user_certificate)
        if not res.ok:
            flash('could not connect with this certificate', category = 'error')
            return render_template("loginCert.html")

        session['uid'] = res.json()['uid']
        session['firstname'] = res.json()['firstname']
        session['lastname'] = res.json()['lastname']
        session['email'] = res.json()['email']
        session['is_admin'] = res.json()['is_admin']
         
        return redirect(url_for('views.home'))
        ################################################
        ################################################
        ################################################
        ## Request to back end to check user info
        ## If success add session info in the cookie : session[’username’] = request.form[’username’]
        ##if error flash an error message: flash('User already exist', category='error')
        ################################################
        ################################################
        ################################################
    return render_template("loginCert.html")


@auth.route('/logout')
def logout():
        ################################################
        ################################################
        ################################################
        ## Only accessible if user is connected
        ## Remove info from the cookies : session.pop('username', None)
        ################################################
        ################################################
        ################################################
        # Only logged in user have acces to this page
    if not ('uid' in session):
        return redirect(url_for('auth.loginPWD'))
        
        
        ################################################
        #For test, to be removed
        ################################################
    session.pop('uid', None)
    session.pop('firstname', None)
    session.pop('lastname', None)
    session.pop('email', None)
    session.pop('is_admin', None)
        
        ################################################
        #End of test, to be removed
        ################################################
    return redirect(url_for('auth.loginPWD'))
