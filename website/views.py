from flask import Blueprint, render_template, request, flash, jsonify, redirect, url_for, session, send_file
from datetime import timedelta
import io
import json
import requests
import uuid
import zipfile

views = Blueprint('views', __name__)
bendAd = "http://backend"

_machine_id = None
def get_machine_id():
    global _machine_id

    if _machine_id is not None:
        return _machine_id

    linux = b""

    # machine-id is stable across boots, boot_id is not.
    for filename in "/etc/machine-id", "/proc/sys/kernel/random/boot_id":
        try:
            with open(filename, "rb") as f:
                value = f.readline().strip()
        except OSError:
            continue

        if value:
            linux += value
            break

    # Containers share the same machine id, add some cgroup
    # information. This is used outside containers too but should be
    # relatively stable across boots.
    try:
        with open("/proc/self/cgroup", "rb") as f:
            linux += f.readline().strip().rpartition(b"/")[2]
    except OSError:
        pass

    if linux:
        _machine_id = linux
    return _machine_id

@views.before_app_first_request 
def make_session_permanent():
    session.permanent = True
    views.permanent_session_lifetime = timedelta(minutes=60)

@views.route('/', methods=['GET'])
def main():
    return redirect(url_for('auth.loginPWD'))


@views.route('/home', methods=['GET', 'POST'])
def home():
    # Only logged in user have acces to this page
    if not ('uid' in session):
        return redirect(url_for('auth.loginPWD'))
        ################################################
        ################################################
        ################################################
        ## Should print info from the cookies that was set at logging
        ## If Post method then change info that are not null in the
        ##if error flash an error message: flash('invalid change', category='error')
        ################################################
        ################################################
        ################################################
    params = {}

    if request.method == 'POST' and session['is_admin'] == 'False':
        email = request.form.get('email')
        first_name = request.form.get('firstName')
        last_name = request.form.get('lastName')

        args = {"email" : email,
                "first_name" : first_name,
                "last_name": last_name,
                "uid" : session['uid']}
        res = requests.post(bendAd + "/update", json = args)
        if not res.ok:
            flash('Could not update data', category = 'error')

    if session['is_admin'] == "True":
        res = requests.get(bendAd + "/admin")
        if not res.ok:
            flash('Admin data could not be fetched', category = 'error')
        else :
            params = res.json()


    return render_template("home.html", response = params, mac_addr=str(uuid.getnode()), machine_id=get_machine_id())

@views.route('/Cert', methods=['GET', 'POST'])
def manageCert():
    # Only logged in user have acces to this pageg
    if not ('uid' in session):
        return redirect(url_for('auth.loginPWD'))

    if session['is_admin'] == 'True':
        return redirect(url_for('views.home'))
        ################################################
        ################################################
        ################################################
        ## if post methpde: Create or delete the certificate based on the choice
        ##if user try to delete a certificate without having one  flash an error message: flash('no existing certificate', category='error')
        ################################################
        ################################################
        ################################################
        
    if request.method == 'POST':
        action = request.form.get('ManageSelect')
        
        if action=="1":
            # Generate a new certificate
            rep = requests.post("http://backend/gen", json={'first_name': session['firstname'], 'last_name': session['lastname'], 'email': session['email'], 'uid': session['uid']})
            if not rep.ok:
                flash('Could not generate the certificate', category = 'error')
                return render_template("manageCert.html")
            sk = rep.json()['secretKey']
            cert = rep.json()['certificate']
            buf = io.BytesIO()
            with zipfile.ZipFile(buf, 'w') as ar:
                ar.writestr("cert.pem", cert)
                ar.writestr("key.pem", sk)
            buf.seek(0)
                
            flash('certificate generated!', category='success')

            return send_file(buf, as_attachment=True, attachment_filename="cert.zip")
            # TODO: find a way to delete the file after it is sent
        if action=="2":
            # Revoke certificate
            cert = request.form.get('Certificate')
            res = requests.post(bendAd + "/revoke", json = cert)
            if not res.ok :
                flash('couldn\'t revoke the certificate', category = 'error')
                return render_template("manageCert.html")
            flash('certificate deleted!', category='success')
            

    return render_template("manageCert.html")

