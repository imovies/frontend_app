from flask import Flask
from os import path
from secrets import token_hex

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = token_hex(16)

    from .views import views
    from .auth import auth

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')

    return app

app = create_app()

from werkzeug.debug import DebuggedApplication
app.wsgi_app = DebuggedApplication(app.wsgi_app, True)
