# The flake file is the entry point for nix commands
{
  description = "iMovies CA Web frontend";

  # Inputs are how Nix can use code from outside the flake during evaluation.
  inputs.fup.url = "github:gytis-ivaskevicius/flake-utils-plus/v1.3.1";

  # Outputs are the public-facing interface to the flake.
  outputs = inputs @ {
    self,
    fup,
    nixpkgs,
    ...
  }:
    fup.lib.mkFlake {
      inherit self inputs;

      supportedSystems = ["x86_64-linux"];

      outputsBuilder = channels: {
        defaultPackage = channels.nixpkgs.callPackage nix/package.nix {};
      };
    };
}
